<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    /**
     *
     * Generate random string
     *
     * @param    int        $length - Length of string (default value: 6)
     * @return   string
     *
     */

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /**
     *
     * Connect to the MySQL DB
     *
     * @return   object     MysqliDb object
     *
     */
    function connectToDB() {
        require_once('MysqliDb.php');

        $DBdata = array(
          "host"  => "localhost",
          "user"  => "root",
          "pass"  => "root",
          "dbname"    => "test"
        );

        return new MysqliDb($DBdata['host'], $DBdata['user'], $DBdata['pass'], $DBdata['dbname']);
    }

    /**
     *
     * Checks and prepares the data for the file
     *
     * @param    array      $post - the $_POST variable what was sent by the form
     * @return   string     prepared string for the file, bool false on error
     *
     */
    function prepareData($post) {
        if (   isset($post['sex'])
            && isset($post['name'])
            && isset($post['email'])
            && isset($post['tel'])
            && isset($post['message'])
        ) {
            $ret = implode("\n", array($post['sex'], $post['name'], $post['email'], $post['tel'],$post['message']));
        } else {
            $ret = false;
        }
        return $ret;
    }


    /**
     *
     * Create new file - save the data
     *
     * @param    string     $str - string to write in the file
     * @return   string     filename with extension, bool false on fail
     *
     */
    function saveToFile($str) {
        $filename = generateRandomString() . ".txt";
        $ret = '';
        // check if file with this name already exists
        if (!file_exists("save/" . $filename)) {
            // file not exists, create and write
            if (file_put_contents("save/" . $filename, $str, LOCK_EX) === false) {
                $ret = false;
            } else {
                $ret = $filename;
            }
        } else {
            // file already exists - call self function
            $ret = saveToFile($str);
        }
        return $ret;
    }


    /**
     *
     * Save filename to database
     *
     * @param    string     $filename - filename
     * @return   bool       true on success, false on error
     *
     */
    function saveToDB($filename) {
        $table = 'task2'; // define table name
        $db = connectToDB(); // Connect to database

        // Prepare data for DB insert
        $data = array(
            'filename' => $filename,
            'time' => time()
        );

        // Insert data
        if ($db->insert($table, $data)) {
            $ret = true;
        } else {
            $ret = false;
        }
        return $ret;
    }



/* *********** */


$error = ''; // Error message container
$data = prepareData($_POST); // prepare the data for file
if ($data) {
    $filename = saveToFile($data); // save the file
    if ($filename) {
        if (!saveToDB($filename)) {
            // error saving to DB
            $error = 'Error saving filename in database.';
        }
    } else {
        $error = 'Error writing to file.';
    }
} else {
    $error = 'Some data are missing.';
}

if (strlen($error) === 0) {
    // No error, return OK
    echo "OK"; // <- JavaScript will check if the returned message is equal to "OK"
} else {
    // There was an error, return the error message
    echo $error;
}