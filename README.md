# Task Nr. 2 #

A form is created in the index.html. You need to work with this file. In the form, every field is required.

## Requirements ##

1. Make a client-side validation with one of your chosen validator plugins (~)

2. Create a **PHP** script in the `/api` folder for processing the form. The script should get the form data through a **POST** ($_POST) request and saves every submit as a separate txt file (separate lines, fieldname: value) in the `/api/save` folder with a filename created from a 6 char random string (0-9A-Za-z for example *apwE4d.txt*).

3. The filname should be saved in a database. Create a database and a table with a structure of "**id | filename | time**" where **id (PRIMARY KEY)** = *auto incremented integer*, **filename** = *the generated filename with extension (VARCHAR)*, **time** - *current timestamp (INT)*

4. Submit the form with AJAX. Display a response message under the form (Error msg if somehow an empty form was submitted, success message if everything was good and the data was saved): [EXAMPLE for AJAX FORM SUBMISSION][5] (~~)

5. Copy a database export (.sql|.zip|.gzip) near the *index.html* files when you are ready.



## Hints/tipps ##
(~) If you don't have a preferred js plugin for validation, you can use [Parsleyjs][1] - Use the styles from the [Bootstrap Material][2] stylesheet (on this page, hovering an element and clicking the "<>" button in the right corner will show you the source-code

(~~) For the error/success messages, use also the styles from the [Bootstrap Material][3] stylesheet

! For the MySQL connection/operations from PHP, you can use [THIS CLASS][4] if you want to save time.

[1]: http://parsleyjs.org/doc/index.html#psly-installation-localization

[2]: http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html#forms
[3]: http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html#indicators

[4]: https://github.com/joshcam/PHP-MySQLi-Database-Class
[5]: https://scotch.io/tutorials/submitting-ajax-forms-with-jquery


