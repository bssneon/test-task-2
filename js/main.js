(function ($, undefined) {
  var $document = $(document);

  var _processForm = function () {
    $('#contactform').submit(function (e) {
      // We are catching the event on form submission and prevent the default action (stop sending http request aka real form submission)
      e.preventDefault();
      var $form = $(this);
      var formData = $form.serialize(); // create url encoded string from form data
      $form.find('button').attr('disabled', true); // disable the button until the form is processed


      // check if there is already a return message alert box, if yes, remove it
      if ($form.find('.return-msg').length > 0) {
        $form.find('.return-msg').remove();
      }

      // if form is valid, send it to our process file
      if ($form.parsley().isValid()) {
        $.ajax({
          url: 'api/process.php', // the file which processes the form
          type: 'POST', // setting the method
          data: formData // the form data which we serialized
        })
        .done(function (msg) {
          // this function is called when the request was successfull
          // every string which is echoed in the process.php will be in the 'msg' variable
          // check if the processing was successfull
          if (msg === 'OK') {
            // create a success alert box to the end of the form with a message and clear the form and enable the button
            $('<div />').addClass('alert alert-success return-msg').text('Well done, the form was saved!').css({'display' : 'none'}).appendTo($form).fadeIn(300);
            $form[0].reset();
            $form.find('button').attr('disabled', false);
          } else {
            // There was an error, create an error alert box to the end of the form with the message which was returned
            $('<div />').addClass('alert alert-danger return-msg').text(msg).css({'display' : 'none'}).appendTo($form).fadeIn(300);
          }
        });
      }

    });
  };

  $document.ready(function () {
    _processForm();
  });


}(window.jQuery));